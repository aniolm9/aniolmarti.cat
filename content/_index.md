+++
+++

Welcome to my home page!

## About me
I am Aniol Martí, a PhD candidate in the [Signal Processing and Communications group](https://spcom.upc.edu/) at the Universitat Politècnica de Catalunya.
In addition, I am studying a MSc in Mathematics at the Universidad Nacional de Educación a Distancia.

## Research
My current research interests are wireless communications, signal processing and information geometry.

Here there is a list of my [research publications](/publications).
You can also check my [Scholar](https://scholar.google.es/citations?user=djkNDlQAAAAJ) or the [official website](https://futur.upc.edu/AniolMartiEspelt) of my university.

## Teaching
I teach undergraduate courses in the [Barcelona School of Telecommunications Engineering](https://telecos.upc.edu/).
Here you can find the full list of my [teaching activities](/teaching).
Other materials are available at [Aprèn](https://apren.upc.edu/ca/professorat/1228490).

### Current courses
<details><summary>230913 - Senyals i Sistemes (SST)</summary>

* [Guia docent](https://www.upc.edu/content/grau/guiadocent/pdf/cat/230913)
* [Pràctiques](https://upcommons.upc.edu/bitstream/handle/2117/398871/practiques_sst_julia.pdf)

</details>
<details><summary>230084 - Circuits i Sistemes Lineals (CSL)</summary>

* [Guia docent](https://www.upc.edu/content/grau/guiadocent/pdf/cat/230084)
* [Apunts](https://upcommons.upc.edu/bitstream/handle/2117/388445/apunts_csl.pdf)

</details>
