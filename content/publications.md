+++
title = "Publications"
+++
# Publications
## Peer-reviewed journal articles
* **A. Martí**, J. Riba, M. Lamarca and X. Gràcia  
"Asymptotic Analysis of Near-Field Coupling in Massive MISO and Massive SIMO Systems"  
_IEEE Communications Letters, vol. 28, pp. 1929-1933_, 2024  
DOI: [10.1109/LCOMM.2024.3416044](https://doi.org/10.1109/LCOMM.2024.3416044)

* M. Vilà-Insa, **A. Martí**, J. Riba and M. Lamarca  
"Quadratic Detection in Noncoherent Massive SIMO Systems over Correlated Channels"  
_IEEE Transactions on Wireless Communications, vol. 23, pp. 14259-14272_, 2024  
DOI: [10.1109/TWC.2024.3411164](https://doi.org/10.1109/TWC.2024.3411164)

* **A. Martí**, F. de Cabrera and J. Riba  
"On the Estimation of Tsallis Entropy and a Novel Information Measure Based on its Properties"  
_IEEE Signal Processing Letters, vol. 30, pp. 818-822_, 2023  
DOI: [10.1109/LSP.2023.3291308](https://doi.org/10.1109/LSP.2023.3291308)

* **A. Martí**, J. Portell, J. Riba and O. Mas  
"Context-Aware Lossless and Lossy Compression of Radio Frequency Signals"  
_Sensors, 23(7)_, 2023  
DOI: [10.3390/s23073552](https://doi.org/10.3390/s23073552)

* **A. Martí**, J. Portell, D. Amblas, F. de Cabrera, M. Vilà-Insa, J. Riba and G. Mitchell  
"Compression of Multibeam Echosounders Bathymetry and Water Column Data"  
_Remote Sensing, 14(9)_, 2022  
DOI: [10.3390/rs14092063](https://doi.org/10.3390/rs14092063)

## Conference proceedings
* **A. Martí**, M. Vilà-Insa, J. Riba and M. Lamarca  
"Constellation Design for Quadratic Detection in Noncoherent Massive SIMO Communications"  
_2024 IEEE 25th International Workshop on Signal Processing Advances in Wireless Communications (SPAWC)_  
DOI: [10.1109/SPAWC60668.2024.10694142](https://doi.org/10.1109/SPAWC60668.2024.10694142)

* J. Portell, **A. Martí**, R. Iudica, D. Evans and V. Zelenevskiy  
"Image and Radio-Frequency Data Compression for OPS-SAT Using FAPEC"  
_8th International Workshop on On-Board Payload Data Compression (OBPDC)_, 2022  
DOI: [10.5281/zenodo.7244849](https://doi.org/10.5281/zenodo.7244849)
