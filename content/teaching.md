+++
title = "Teaching"
+++
# Teaching
<!-- ## Teaching assignment -->
| Date | University | School/Faculty | Course |
|---|---|---|---|
| 2024 Spring | UPC | ETSETB | [230084 - Linear Circuits and Systems](https://www.upc.edu/content/grau/guiadocent/pdf/ang/230084) |
| 2023 Fall | UPC | ETSETB | [230913 - Signals and Systems](https://www.upc.edu/content/grau/guiadocent/pdf/ang/230913) |
| 2023 Spring | UPC | ETSETB | [230084 - Linear Circuits and Systems](https://www.upc.edu/content/grau/guiadocent/pdf/ang/230084) |
